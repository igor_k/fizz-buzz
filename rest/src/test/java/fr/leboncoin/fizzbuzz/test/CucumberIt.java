package fr.leboncoin.fizzbuzz.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true, features = "classpath:cucumber", glue = { "fr.leboncoin.fizzbuzz.test.step" }, plugin = "pretty")
public class CucumberIt {
}
