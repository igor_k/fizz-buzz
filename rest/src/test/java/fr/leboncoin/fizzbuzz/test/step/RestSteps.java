package fr.leboncoin.fizzbuzz.test.step;

import static io.restassured.RestAssured.given;

import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.leboncoin.fizzbuzz.controller.util.Endpoint;
import fr.leboncoin.fizzbuzz.test.util.TestUtils;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestSteps {

	@LocalServerPort
	private int port;

	private RequestSpecification requestSpecification;

	private Response response;

	@Before
	public void setUp() {
		RestAssured.port = port;
		requestSpecification = given();
		response = null;
	}

	@Given("request parameter {word} with value {word}")
	public void setParameter(String parameterName, String value) {
		if (!value.equals(TestUtils.UNDEFINED)) {
			String parameterValue = TestUtils.parseString(value);
			requestSpecification = requestSpecification.param(parameterName, parameterValue);
		}
	}

	@When("the user gets fizz-buzz strings with given parameters")
	public void getFizzBuzzStrings() {
		response = requestSpecification.when().get(Endpoint.FIZZ_BUZZ_STRINGS);
	}

	@When("the user requests a non-existent resource")
	public void getNonExistentResource() {
		response = requestSpecification.when().get("test");
	}

	@Then("the response status code is equal to {int}")
	public void checkStatus(Integer statusCode) {
		response.then().assertThat().statusCode(statusCode);
	}

	@Then("the JSON response result is equal to {string}")
	public void checkResult(String jsonFilePath) throws Exception {
		String json = TestUtils.getFileContent(jsonFilePath);
		JSONAssert.assertEquals(json, response.getBody().asString(), true);
	}

}
