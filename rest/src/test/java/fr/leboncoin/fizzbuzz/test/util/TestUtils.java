package fr.leboncoin.fizzbuzz.test.util;

import java.util.Scanner;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

public final class TestUtils {

	public static final String UNDEFINED = "undefined";

	private TestUtils() {
	}

	public static String parseString(String value) {
		ExpressionParser parser = new SpelExpressionParser();
		TemplateParserContext templateContext = new TemplateParserContext();
		EvaluationContext evaluationContext = new StandardEvaluationContext(new TestContext());
		return parser.parseExpression(value, templateContext).getValue(evaluationContext, String.class);
	}

	public static String getFileContent(String filePath) {
		try (Scanner s = new Scanner(Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath))) {
			s.useDelimiter("\\A");
			return s.hasNext() ? s.next() : "";
		}
	}

	public static class TestContext {

		public String getString(int length) {
			return new String(new char[length]).replace('\0', 'a');
		}

	}
}
