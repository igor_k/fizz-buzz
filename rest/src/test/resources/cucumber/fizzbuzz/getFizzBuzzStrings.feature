Feature: fizz-buzz strings can be retrieved

Scenario Outline: <title> case

	Given request parameter fizzString with value <fizzString>
	
	And request parameter buzzString with value <buzzString>
	
	And request parameter fizzNumber with value <fizzNumber>
	
	And request parameter buzzNumber with value <buzzNumber>
	
	And request parameter limit with value <limit>
	
	When the user gets fizz-buzz strings with given parameters
	
	Then the response status code is equal to <httpCode>
	
	And the JSON response result is equal to "cucumber/fizzbuzz/expectedresponse/<responseFileName>.json"
	
	Examples: 
		|	title					|	fizzString			|	buzzString			|	fizzNumber	|	buzzNumber	|	limit		|	httpCode	|	responseFileName	|
		|	Nominal					|	zzif				|	zzub				|	3			|	5			|	16			|	200			|	nominal				|
		|	Null fizzString			|	#{null}				|	zzub				|	3			|	5			|	16			|	200			|	nullFizzString		|
		|	Null buzzString			|	zzif				|	#{null}				|	3			|	5			|	16			|	200			|	nullBuzzString		|
		|	Too long fizzString		|	#{getString(257)}	|	zzub				|	3			|	5			|	10			|	400			|	tooLongFizzString	|
		|	Undefined buzzString	|	zzif				|	undefined			|	3			|	5			|	10			|	400			|	undefinedBuzzString	|
		|	Too long buzzString		|	zzif				|	#{getString(257)}	|	3			|	5			|	10			|	400			|	tooLongBuzzString	|
		|	Undefined fizzNumber	|	zzif				|	zzub				|	undefined	|	5			|	10			|	400			|	undefinedFizzNumber	|
		|	Null fizzNumber			|	zzif				|	zzub				|	#{null}		|	5			|	10			|	400			|	nullFizzNumber		|
		|	Too small fizzNumber	|	zzif				|	zzub				|	-1			|	5			|	10			|	400			|	tooSmallFizzNumber	|
		|	Too large fizzNumber	|	zzif				|	zzub				|	10000001	|	5			|	10			|	400			|	tooLargeFizzNumber	|
		|	Undefined buzzNumber	|	zzif				|	zzub				|	3			|	undefined	|	10			|	400			|	undefinedBuzzNumber	|
		|	Null buzzNumber			|	zzif				|	zzub				|	3			|	#{null}		|	10			|	400			|	nullBuzzNumber		|
		|	Too small buzzNumber	|	zzif				|	zzub				|	3			|	0			|	10			|	400			|	tooSmallBuzzNumber	|
		|	Too large buzzNumber	|	zzif				|	zzub				|	3			|	10000001	|	10			|	400			|	tooLargeBuzzNumber	|
		|	Undefined limit			|	zzif				|	zzub				|	3			|	5			|	undefined	|	400			|	undefinedLimit		|
		|	Null limit				|	zzif				|	zzub				|	3			|	5			|	#{null}		|	400			|	undefinedLimit		|
		|	Too small limit			|	zzif				|	zzub				|	3			|	5			|	-1			|	400			|	tooSmallLimit		|
		|	Too large limit			|	zzif				|	zzub				|	3			|	5			|	10000001	|	400			|	tooLargeLimit		|