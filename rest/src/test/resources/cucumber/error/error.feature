Feature: error can be retrieved

Scenario: non-existent resource case
	
	When the user requests a non-existent resource
	
	Then the response status code is equal to 404
	
	And the JSON response result is equal to "cucumber/error/expectedresponse/notFound.json"