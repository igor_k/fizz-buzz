package fr.leboncoin.fizzbuzz.controller.exception;

public class ExceptionResponse {

	private Integer status;

	private String message;

	private String details;

	public ExceptionResponse(Integer status, String message, String details) {
		super();
		this.status = status;
		this.message = message;
		this.details = details;
	}

	public Integer getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}

}
