package fr.leboncoin.fizzbuzz.controller.fizzbuzz;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.leboncoin.fizzbuzz.controller.util.Endpoint;
import fr.leboncoin.fizzbuzz.service.fizzbuzz.FizzBuzzService;

@RestController
public class FizzBuzzController {

	@Autowired
	private FizzBuzzService fizzBuzzService;

	@GetMapping(value = Endpoint.FIZZ_BUZZ_STRINGS, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<String> getFizzBuzzStrings(@Valid FizzBuzzRequest request) {
		return fizzBuzzService.getFizzBuzzStrings(request.getFizzString(), request.getBuzzString(), request.getFizzNumber(),
				request.getBuzzNumber(), request.getLimit());
	}

}
