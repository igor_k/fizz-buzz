package fr.leboncoin.fizzbuzz.controller.fizzbuzz;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FizzBuzzRequest {

	@NotNull(message = "fizzString is required")
	@Size(max = 256, message = "fizzString cannot be longer than 256 characters")
	private String fizzString;

	@NotNull(message = "buzzString is required")
	@Size(max = 256, message = "buzzString cannot be longer than 256 characters")
	private String buzzString;

	@NotNull(message = "fizzNumber is required")
	@Min(value = 1, message = "fizzNumber cannot be less than 1")
	@Max(value = 1000000, message = "fizzNumber cannot be greater than 1000000")
	private Integer fizzNumber;

	@NotNull(message = "buzzNumber is required")
	@Min(value = 1, message = "buzzNumber cannot be less than 1")
	@Max(value = 1000000, message = "buzzNumber cannot be greater than 1000000")
	private Integer buzzNumber;

	@NotNull(message = "limit is required")
	@Min(value = 0, message = "limit cannot be less than 0")
	@Max(value = 1000000, message = "limit cannot be greater than 1000000")
	private Integer limit;

	public String getFizzString() {
		return fizzString;
	}

	public void setFizzString(String fizzString) {
		this.fizzString = fizzString;
	}

	public String getBuzzString() {
		return buzzString;
	}

	public void setBuzzString(String buzzString) {
		this.buzzString = buzzString;
	}

	public Integer getFizzNumber() {
		return fizzNumber;
	}

	public void setFizzNumber(Integer fizzNumber) {
		this.fizzNumber = fizzNumber;
	}

	public Integer getBuzzNumber() {
		return buzzNumber;
	}

	public void setBuzzNumber(Integer buzzNumber) {
		this.buzzNumber = buzzNumber;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
