package fr.leboncoin.fizzbuzz.controller.exception;

import java.util.Comparator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

	@ExceptionHandler(BindException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentTypeMismatchException(BindException exception, WebRequest webRequest) {
		LOGGER.error("Bad request", exception);

		Optional<String> message = exception.getAllErrors().stream().sorted(Comparator.comparing(ObjectError::getDefaultMessage))
				.map(ObjectError::getDefaultMessage).findFirst();

		return new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), message.orElse("Bad request"), webRequest.getDescription(false));
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleAllException(Exception exception, WebRequest request) {
		LOGGER.error("Internal server error", exception);

		return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), request.getDescription(false));
	}
}
