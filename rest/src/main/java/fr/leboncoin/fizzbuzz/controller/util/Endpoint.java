package fr.leboncoin.fizzbuzz.controller.util;

public final class Endpoint {

	public static final String ERROR = "/error";

	public static final String FIZZ_BUZZ_STRINGS = "/fizzBuzzStrings";

	private Endpoint() {
	}
}
