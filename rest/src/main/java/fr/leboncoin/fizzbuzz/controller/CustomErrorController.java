package fr.leboncoin.fizzbuzz.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import fr.leboncoin.fizzbuzz.controller.exception.ExceptionResponse;
import fr.leboncoin.fizzbuzz.controller.util.Endpoint;

/**
 * Custom implementation of the {@link ErrorController @ErrorController}
 * interface which can map to '/error'
 */
@Controller
public class CustomErrorController implements ErrorController {

	@Autowired
	private ErrorAttributes errorAttributes;

	@RequestMapping(value = Endpoint.ERROR, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ExceptionResponse> error(WebRequest webRequest, HttpServletResponse response) {
		Map<String, ?> attributes = errorAttributes.getErrorAttributes(webRequest, false);
		String message = (String) attributes.get("error");
		ExceptionResponse exceptionResponse = new ExceptionResponse(response.getStatus(), message, webRequest.getDescription(false));

		return ResponseEntity.status(response.getStatus()).body(exceptionResponse);
	}

	@Override
	public String getErrorPath() {
		return Endpoint.ERROR;
	}

}
