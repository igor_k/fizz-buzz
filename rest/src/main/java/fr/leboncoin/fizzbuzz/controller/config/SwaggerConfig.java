package fr.leboncoin.fizzbuzz.controller.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${info.application.name}")
	private String applicationName;

	@Value("${info.application.description}")
	private String applicationDescription;

	@Value("${info.application.version}")
	private String applicationVersion;

	@Bean
	public Docket api() {
		ApiInfo apiInfo = new ApiInfo(applicationName, applicationDescription, applicationVersion, null, null, null, null,
				Collections.emptyList());

		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo);
	}

}
