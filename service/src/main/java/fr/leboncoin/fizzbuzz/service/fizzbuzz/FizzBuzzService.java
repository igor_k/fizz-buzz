package fr.leboncoin.fizzbuzz.service.fizzbuzz;

import java.util.List;

public interface FizzBuzzService {

	List<String> getFizzBuzzStrings(String fizzString, String buzzString, int fizzNumber, int buzzNumber, int limit);

}
