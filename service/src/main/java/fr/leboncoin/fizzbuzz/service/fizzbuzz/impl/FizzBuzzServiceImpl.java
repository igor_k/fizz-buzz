package fr.leboncoin.fizzbuzz.service.fizzbuzz.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.leboncoin.fizzbuzz.service.fizzbuzz.FizzBuzzService;

@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {

	@Override
	public List<String> getFizzBuzzStrings(String fizzString, String buzzString, int fizzNumber, int buzzNumber, int limit) {
		if (fizzString == null) {
			throw new IllegalArgumentException("Null fizzString");
		}

		if (buzzString == null) {
			throw new IllegalArgumentException("Null buzzString");
		}

		if (fizzNumber < 1) {
			throw new IllegalArgumentException("fizzNumber is less than 1");
		}

		if (buzzNumber < 1) {
			throw new IllegalArgumentException("buzzNumber is less than 1");
		}

		if (limit < 0) {
			throw new IllegalArgumentException("limit is less than 0");
		}

		List<String> result = new ArrayList<>(limit);

		for (int i = 1; i <= limit; i++) {
			result.add(getFizzBuzzString(i, fizzString, buzzString, fizzNumber, buzzNumber));
		}

		return result;
	}

	private String getFizzBuzzString(int number, String fizzString, String buzzString, int fizzNumber, int buzzNumber) {
		StringBuilder stringBuilder = new StringBuilder();

		if (number % fizzNumber > 0 && number % buzzNumber > 0) {
			stringBuilder.append(number);
		} else {
			if (number % fizzNumber == 0) {
				stringBuilder.append(fizzString);
			}

			if (number % buzzNumber == 0) {
				stringBuilder.append(buzzString);
			}
		}

		return stringBuilder.toString();
	}
}
