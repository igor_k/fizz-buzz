package fr.leboncoin.fizzbuzz.service.fizzbuzz;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fr.leboncoin.fizzbuzz.service.fizzbuzz.FizzBuzzService;
import fr.leboncoin.fizzbuzz.service.fizzbuzz.impl.FizzBuzzServiceImpl;

public class FizzBuzzServiceUt {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private FizzBuzzService fizzBuzzService;

	@Before
	public void setUp() {
		fizzBuzzService = new FizzBuzzServiceImpl();
	}

	@Test
	public void getFizzBuzzStrings_nominalCase() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", 3, 5, 36);

		List<String> expectedResult = Arrays.asList("1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13",
				"14", "fizzbuzz", "16", "17", "fizz", "19", "buzz", "fizz", "22", "23", "fizz", "buzz", "26", "fizz", "28", "29",
				"fizzbuzz", "31", "32", "fizz", "34", "buzz", "fizz");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_oneResultString() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", 3, 5, 1);

		List<String> expectedResult = Arrays.asList("1");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_emptyResult() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", 3, 5, 0);
		assertThat(result).isEmpty();
	}

	@Test
	public void getFizzBuzzStrings_onlyFizzResultStrings() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz2", "buzz2", 2, 10, 9);

		List<String> expectedResult = Arrays.asList("1", "fizz2", "3", "fizz2", "5", "fizz2", "7", "fizz2", "9");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_onlyBuzzResultStrings() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz2", "buzz2", 10, 3, 9);

		List<String> expectedResult = Arrays.asList("1", "2", "buzz2", "4", "5", "buzz2", "7", "8", "buzz2");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_onlyFizzBuzzResultStrings() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz3", "buzz3", 2, 2, 9);

		List<String> expectedResult = Arrays.asList("1", "fizz3buzz3", "3", "fizz3buzz3", "5", "fizz3buzz3", "7", "fizz3buzz3", "9");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_emptyFizzString() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("", "buzz", 3, 5, 9);

		List<String> expectedResult = Arrays.asList("1", "2", "", "4", "buzz", "", "7", "8", "");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_emptyBuzzString() {
		List<String> result = fizzBuzzService.getFizzBuzzStrings("fizz", "", 3, 5, 10);

		List<String> expectedResult = Arrays.asList("1", "2", "fizz", "4", "", "fizz", "7", "8", "fizz", "");

		assertThat(result).containsExactlyElementsOf(expectedResult);
	}

	@Test
	public void getFizzBuzzStrings_nullFizzString() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Null fizzString");
		fizzBuzzService.getFizzBuzzStrings(null, "buzz", 2, 6, 7);
	}

	@Test
	public void getFizzBuzzStrings_nullBuzzString() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Null buzzString");
		fizzBuzzService.getFizzBuzzStrings("fizz", null, 2, 6, 7);
	}

	@Test
	public void getFizzBuzzStrings_tooSmallBuzzNumber() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("buzzNumber is less than 1");
		fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", 3, -5, 2);
	}

	@Test
	public void getFizzBuzzStrings_tooSmallFizzNumber() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("fizzNumber is less than 1");
		fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", -3, 5, 2);
	}

	@Test
	public void getFizzBuzzStrings_negativeLimit() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("limit is less than 0");
		fizzBuzzService.getFizzBuzzStrings("fizz", "buzz", 3, 5, -2);
	}
}
