# Simple fizz-buzz REST server

## How to Run

* Clone this repository 
* Make sure you are using JDK 8 or higher. To check, run ```java -version```
* Build the project and run the tests by running ```./gradlew build```
* Run the application by running: ```java -jar rest/build/libs/fizz-buzz-1.0.0.jar```
* Check the stdout or logs/app.log file to make sure that the application is up and running

## Management services

```
http://localhost:8080/health
http://localhost:8080/info
```

## API documentation

```
http://localhost:8080/v2/api-docs
```